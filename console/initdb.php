<?php

try {

    $realpath = realpath(dirname(__FILE__));
    define('PUBLIC_PATH', $realpath);
    define('DS', DIRECTORY_SEPARATOR);
    define('APP_PATH', $realpath . DS. '..' . DS . 'application' . DS);

    include(APP_PATH . DS . 'config' . DS . 'err_handler.php');
    include(APP_PATH . DS . 'config' . DS . 'autoloader.php');

    $config = (array)include(APP_PATH . DS . 'config' . DS . 'config.php');

    /** @var \Lib\Core\Application\Application $application */
    $application = new \Lib\Core\Application\Application(
        \Lib\Core\Application\ApplicationInterface::MODECLI,
        new \Lib\Core\Bootstrap\CoreBootstrap(),
        $config
    );

    $application->boot();

    // Database creation example:
    // CREATE DATABASE `nvertisett` /*!40100 DEFAULT CHARACTER SET utf8 */


    /* @var \Lib\Vendor\PHPPDOWrapper\DB $db */
    $db = $application->getServiceManager()->getService('db');

    // Disable foreign keys constrains for this session
    $db->run("SET FOREIGN_KEY_CHECKS = 0");


    /**
     * Process 'article' table and insert fixtured data
     */

    $sql = "DROP TABLE IF EXISTS `article`;
            CREATE TABLE `article` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(250) NOT NULL,
              `content` text,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    
    ";

    $db->run($sql);

    $sql = "INSERT INTO article (title, content) VALUES ('Hi, I am a lonely test article', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.')";

    $db->run($sql);


    /**
     * Process 'comment' table and insert fixtured data
     */

    $sql = "DROP TABLE IF EXISTS `comment`;
            CREATE TABLE `comment` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `article_id` int(11) DEFAULT NULL,
              `title` varchar(250) NOT NULL,
              `content` text,
              `author_name` varchar(50) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `comment_article_id_index` (`article_id`),
              CONSTRAINT `comment_article_id_fk` FOREIGN KEY (`article_id`) 
                REFERENCES `article` (`id`) ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    
    ";

    $db->run($sql);


    $sql = "INSERT INTO comment (article_id, title, content, author_name) 
              VALUES 
              (1, 'Test Comment Title 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry :)', 'Guest'),
              (1, 'Test Comment Title 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry :(', 'Anonimous'),
              (1, 'Test Comment Title 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry :)', 'Guest'),
              (1, 'Test Comment Title 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry :(', 'Guest'),
              (1, 'Test Comment Title 5', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry :)', 'Anonimous');
    
    ";

    $db->run($sql);


    /**
     * Process 'observer' table and insert fixtured data
     */

    $sql = "DROP TABLE IF EXISTS `observer`;
            CREATE TABLE `observer` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `event_name` varchar(50) NOT NULL,
              `observers` text COMMENT 'serialized array of Observers',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ";

    $db->run($sql);

    $fooObserver = new \Module\Test\Observer\Foo();
    $barObserver = new \Module\Test\Observer\Bar();

    // One observer for testEventOne Event
    $testEventOneSubs = array($fooObserver);

    // Two observers for testEventTwo Event
    $testEventTwoSubs = array($fooObserver, $barObserver);


    $sql = "INSERT INTO observer (event_name, observers) VALUES 
            ('testEventOne', '" . base64_encode(serialize($testEventOneSubs)) . "'),
            ('testEventTwo', '" . base64_encode(serialize($testEventTwoSubs)) . "');
    ";

    $db->run($sql);


} catch (\Exception $e) {
    /** Process all the exceptions here by type */
    var_dump($e);
}