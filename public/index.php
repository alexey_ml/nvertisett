<?php

/**
 * This is a test application
 *
 * It has been built without of any Framework, from the scratch exactly for the test task.
 * It uses the MVC pattern and driven by Dispatcher (Mediator) and EventManager (Observer)
 *
 ** or in the browser like: http://host.local/ff/public/index.php (it will emulate the ARGV)
 * Resulting reports are in /public/Report
 *
 * @author Moldavanov Alexey lunizz@gmail.com Skype: lunizz_odessa
 */



try {

    $realpath = realpath(dirname(__FILE__));
    define('PUBLIC_PATH', $realpath);
    define('DS', DIRECTORY_SEPARATOR);
    define('APP_PATH', $realpath . DS. '..' . DS . 'application' . DS);

    include(APP_PATH . DS . 'config' . DS . 'err_handler.php');
    include(APP_PATH . DS . 'config' . DS . 'autoloader.php');

    $config = (array)include(APP_PATH . DS . 'config' . DS . 'config.php');

    /**
     * Build and run Application
     * @var \Lib\Core\Application\Application $application
     */
    $application = new \Lib\Core\Application\Application(
        \Lib\Core\Application\ApplicationInterface::MODEHTTP,
        new \Lib\Core\Bootstrap\CoreBootstrap(),
        $config
    );

    $application->boot()->run();

} catch (\Exception $e) {
    /** Process all the exceptions here by type */
    var_dump($e);
}