define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.ItemView.extend({

        tagName: 'tr',
        template: '#comment-view'

    });

});