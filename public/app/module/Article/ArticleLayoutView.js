define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.LayoutView.extend({

        el: '#main-content',
        template: '#main-layout-view-template',

        regions: {
            articleRegion: '#article',
            commentsRegion: '#comments',
            formRegion: '#comment-form'
        }

    });

});