define([

    'backbone'

], function(

    Backbone

) {

    return Backbone.Model.extend({

        defaults: {
            articleId: 0,
            title: '',
            content: '',
            author: ''
        },

        actionUrls: {
            save: '/api/model'
        }

    });

});