define([

    'marionette', 'Article/CommentView'

], function(

    Marionette, CommentView

) {

    return Marionette.CollectionView.extend({

        childView: CommentView,
        tagName: 'table',
        className : 'table table-striped',

        draw: function() {
            var tSelf = this;

            this.collection.fetch()
                .then(function(){
                    tSelf.$el.empty();
                    tSelf.render();
                    window.App.LayoutViews.article
                        .getRegion('commentsRegion')
                        .show(window.App.CollectionsViews.comments, {});
                })

                .fail(function(){
                    alert(Messages.FetchOperationFailed); // Or redirect to err page
                });
        }


    });

});