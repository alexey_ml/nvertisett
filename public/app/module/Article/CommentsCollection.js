define([

    'backbone', 'Article/CommentModel'

], function(

    Backbone, CommentModel

) {

    return Backbone.Collection.extend({

        model: CommentModel,
        url: '/api/comments'

    });

});