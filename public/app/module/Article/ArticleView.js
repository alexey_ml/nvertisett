define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.ItemView.extend({

        template: "#article-content",
        tagName: 'section'

    });

});