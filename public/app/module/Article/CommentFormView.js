define([

    'marionette'

], function(

    Marionette

) {

    return Marionette.ItemView.extend({

        tagName: 'div',
        className: 'well',
        template: '#comment-form-tpl',

        events: {
            'click #comment-submit': 'save',
            'click #comment-smiles-process': 'processSmiles'
        },

        /**
         * Save a new Comment
         */
        save: function(e) {
            this.model
                .set('articleId', 1)
                .set('title', $('#comment-title').val())
                .set('content', $('#comment-content').val())
                .set('author', $('#comment-username').val())
                .save({}, {
                    url: '/api/comment/add',

                    success: function( model, response ){
                        window.App.vent.trigger('postCommentAdded');
                    },

                    error: function( model, response ){
                        alert('Error');
                    }

                });
        },

        /**
         * Replace text smiles in the Comments with images
         */
        processSmiles: function(){
            console.log('Processing Smiles');
            window.App.vent.trigger('processSmilesInComments');
        },

        resetForm: function() {
            $('#comment-title').val('');
            $('#comment-content').val('');
            $('#comment-username').val('');
        }

    });

});
