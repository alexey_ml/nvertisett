define([

    'marionette', 'i18n/i18n!i18n/nls/messages',
    'Article/ArticleLayoutView', 'Article/CommentsCollection',
    'Article/CommentsView', 'Article/CommentFormView', 'Article/CommentModel'

], function(

    Marionette, Messages,
    ArticleLayoutView, CommentsCollection,
    CommentsView, CommentFormView, CommentModel

) {

    return Marionette.Controller.extend({

        home: function() {
            console.log('Home Route Invoked');

            /** Render Article */
            window.App.LayoutViews.article = new ArticleLayoutView();
            window.App.LayoutViews.article.render();
            window.App.LayoutViews.article.getRegion('articleRegion').show(window.App.ItemViews.ArticleView, {});

            /** Render Form */
            window.App.ItemViews.commentForm = new CommentFormView({model: new CommentModel()});
            window.App.ItemViews.commentForm.render();
            window.App.LayoutViews.article.getRegion('formRegion').show(window.App.ItemViews.commentForm, {});

            /** Render Comments */
            window.App.Collections.comments = new CommentsCollection();
            window.App.CollectionsViews.comments = new CommentsView({ collection: window.App.Collections.comments });

            window.App.CollectionsViews.comments.draw();
        }

    });

});