define([

    'marionette', 'App/MainController'

], function(

    Marionette, MainController

) {

    return Marionette.AppRouter.extend({

        controller: new MainController(),

        appRoutes: {

            '': 'home',
            'home': 'home',
            '*notFound' : 'home'

        }

    });

});