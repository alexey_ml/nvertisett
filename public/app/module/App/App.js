define([

    'marionette', 'App/Router', 'Article/ArticleView'

], function(

    Marionette, Router, ArticleView

) {

    return Marionette.Application.extend({

        region: '#main-content',

        LayoutViews: {},
        ItemViews: {},
        CollectionsViews: {},
        Models: {},
        Collections: {},
        Router: {},

        RequestFormat: {
            JSON: 'application/json',
            XML: 'application/xml'
        },

        SupportedLang: {
            EN: 'en'
        },

        AcceptedLang: {
           en: 'en;q=1'
        },

        /**
         * Setup global for App data
         * @returns {App}
         */
        setupEssential: function(){
            var tSelf = this;

            $( document ).ajaxSend(function( event, jqXHR ) {
                jqXHR.setRequestHeader('Accept-Language', tSelf.AcceptedLang['en']);
                jqXHR.setRequestHeader('Accept', tSelf.RequestFormat.JSON);
                jqXHR.setRequestHeader('X-Accept-Version', 'v1');

            });

            return this;
        },

        /**
         * Setup Common Components
         * @returns {App}
         */
        setupComponents: function() {
            this.ItemViews.ArticleView = new ArticleView();
            // this.ItemViews.footer = new FooterView();

            this.Router = new Router();
            return this;
        },

        /**
         * Auto Hook
         */
        initialize: function() {
            var tSelf = this;

            this
                .setupEssential()
                .setupComponents();

            /**
             * Set Event Listeners
             */

            /** Re-draw comments after of a new adding */
            this.vent.on('postCommentAdded', function() {

                tSelf.CollectionsViews.comments.draw();
                tSelf.ItemViews.commentForm.resetForm();
            });

            /** Replace text images :) and :( with images: smile.gif and sad.gif */
            this.vent.on('processSmilesInComments', function() {

                var emotes = [
                    [':\\\)', 'smile.gif'],
                    [':\\\(', 'sad.gif']
                ];

                var content = tSelf.CollectionsViews.comments.$el.html();
                tSelf.CollectionsViews.comments.$el.empty();

                for(var i = 0; i < emotes.length; i++) {
                    content = content.replace(
                        new RegExp(emotes[i][0], 'gi'),
                        '<img src="/images/emotes/' + emotes[i][1] + '">'
                    );
                }

                tSelf.CollectionsViews.comments.$el.html(content);

            });

        }

    });

});