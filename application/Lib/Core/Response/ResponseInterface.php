<?php

namespace Lib\Core\Response;

/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 16.06.16
 * Time: 19:51
 */
interface ResponseInterface
{
    const RESPONSE_OK = 'HTTP/1.1 200 OK';
    const RESPONSE_NOTFOUND = 'HTTP/1.1 404 Not Found';
    const RESPONSE_SERVERROR = 'HTTP/1.1 500 Internal Server Error';

    const CONTTYPE_HTML = 'Content-Type: text/html; charset=utf-8';
    const CONTTYPE_JSON = 'Content-Type: application/json';

    public function setStatus($status);
    public function addHeader($key, $value);
    public function getHeaders();
    public function getHeader($key);
    public function removeHeader($key);
    public function setContentType($value);
    public function setContent($value);
}