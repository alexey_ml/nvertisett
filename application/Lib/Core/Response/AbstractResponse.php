<?php

namespace Lib\Core\Response;
use Lib\Core\ServiceManager\SMResourceInterface;

/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 16.06.16
 * Time: 19:50
 */
class AbstractResponse implements ResponseInterface, SMResourceInterface
{
    /**
     * @var array $this->headers
     */
    protected $headers = array(0);

    /**
     * @var String $this->status
     */
    protected $status;

    /**
     * @var String $this->content
     */
    protected $content;

    /**
     * @var String $this->contentType
     */
    protected $contentType;

    /**
     * @param string $key
     * @return string
     */
    public function getHeader($key)
    {
        if (isset($this->headers[$key])) {
            return $this->headers[$key];
        }
        
        return '';
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }


    /**
     * @param string $key
     * @param string $value
     * @return AbstractResponse
     */
    public function addHeader($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @return bool
     */
    public function removeHeader($key)
    {
        if (isset($this->headers[$key])) {
            unset($this->headers[$key]);
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return AbstractResponse
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return AbstractResponse
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param mixed $contentType
     * @return AbstractResponse
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
        return $this;
    }




}