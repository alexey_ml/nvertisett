<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/31/15
 * Time: 4:51 PM
 */

namespace Lib\Core\Controller;


use Lib\Core\EventManager\ObserverInterface;
use Lib\Core\Router\MatchedRoute;
use Lib\Core\ServiceManager\ServiceManager;
use Lib\Core\ServiceManager\SMServiceInterface;

class AbstractController implements SMServiceInterface, ObserverInterface {

    /**
     * @var ServiceManager $this->serviceManager
     */
    protected $serviceManager;

    public function fireByEvent($event, $serviceManager = null)
    {

        /* @var MatchedRoute $matchedRoute */
        $matchedRoute = $this->serviceManager->getResource('matchedRoute');
        if (empty($matchedRoute)) {
            throw new \RuntimeException('Matched route not found!');
        }

        $action = $matchedRoute->getActionName();
        if (empty($action)) {
            throw new \RuntimeException('No action been calculated');
        }

        $methodName = $action . 'Action';

        $reflObject = new \ReflectionObject($this);
        if(!$reflObject->hasMethod($methodName)) {
            throw new \RuntimeException('Action ' . $methodName . ' not found');
        }

    // Run the action
        $this->{$methodName}();
    }

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }



}