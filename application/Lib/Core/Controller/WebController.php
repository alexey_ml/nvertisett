<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 17.06.16
 * Time: 10:08
 */

namespace Lib\Core\Controller;


use Lib\Core\Response\Response;

class WebController extends AbstractController
{
    /**
     * @param Response $response
     */
    protected function sendResponse($response)
    {
        header($response->getContentType());
        header($response->getStatus());
        echo $response->getContent();
    }

    /**
     * @return bool
     */
    protected function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    /**
     * @return bool
     */
    protected function isGet()
    {
        return $_SERVER['REQUEST_METHOD'] == 'GET';
    }

    /**
     * @return bool
     */
    protected function isPut()
    {
        return $_SERVER['REQUEST_METHOD'] == 'PUT';
    }

    /**
     * @return bool
     */
    protected function isDelete()
    {
        return $_SERVER['REQUEST_METHOD'] == 'DELETE';
    }

    /**
     * @return bool
     */
    protected function isPatch()
    {
        return $_SERVER['REQUEST_METHOD'] == 'PATCH';
    }
}