<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/31/15
 * Time: 4:56 PM
 */

namespace Lib\Core\Controller;

use Lib\Core\Router\MatchedRoute;
use Lib\Core\ServiceManager\ServiceManager;

class ControllerFactory {

    /**
     * @param ServiceManager $serviceManager
     * @return AbstractController
     */
    public static function factory($serviceManager)
    {
        /* @var MatchedRoute $mRoute */
        $mRoute = $serviceManager->getResource('matchedRoute');
        $contrName = '\\Module\\' . $mRoute->getModuleName() . '\\Controller\\' . $mRoute->getControllerName();
        $controller = new $contrName();

        return $controller;
    }
}