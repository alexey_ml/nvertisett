<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:39 PM
 */

namespace Lib\Core\ServiceManager;


interface SMInterface {
    /**
     * @param string $name
     * @param SMResourceInterface $resource
     * @return bool
     */
    public function addResource($name, SMResourceInterface $resource);

    /**
     * @param string $name
     * @param SMServiceInterface $service
     * @return bool
     */
    public function addService($name, SMServiceInterface $service);

    /**
     * @param string $name
     * @return SMResourceInterface
     */
    public function getResource($name);

    /**
     * @param string $name
     * @return SMServiceInterface
     */
    public function getService($name);

    /**
     * @param string $name
     * @return bool
     */
    public function removeResource($name);

    /**
     * @param string $name
     * @return bool
     */
    public function removeService($name);
}