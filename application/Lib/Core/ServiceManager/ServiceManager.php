<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:38 PM
 */

namespace Lib\Core\ServiceManager;


class ServiceManager implements SMInterface {

    /* @var \Lib\Core\Tools\Container $this->resources */
    protected $resources;

    /* @var \Lib\Core\Tools\Container $this->services */
    protected $services;

    /**
     * @param \Lib\Core\Tools\Container $resources
     * @param \Lib\Core\Tools\Container $services
     */
    public function __construct($resources, $services)
    {
        $this->resources = $resources;
        $this->services = $services;
    }

    /**
     * @param string $name
     * @param SMResourceInterface $resource
     * @return bool
     */
    public function addResource($name, SMResourceInterface $resource)
    {
        $this->resources->offsetSet($name, $resource);
        return true;
    }

    /**
     * @param string $name
     * @param SMServiceInterface $service
     * @return bool
     */
    public function addService($name, SMServiceInterface $service)
    {
        $this->services->offsetSet($name, $service);
        return true;
    }


    /**
     * @param string $name
     * @return bool|SMResourceInterface
     */
    public function getResource($name)
    {
        return $this->resources->get($name);
    }

    /**
     * @param string $name
     * @return bool|SMServiceInterface
     */
    public function getService($name)
    {
        return $this->services->get($name);
    }

    public function removeResource($name)
    {
        return true;
    }

    public function removeService($name)
    {
        return true;
    }

}