<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 8:07 PM
 */

namespace Lib\Core\Request;

use Lib\Core\ServiceManager\SMResourceInterface;

interface RequestInterface extends SMResourceInterface {


    public function getPost($key, $default);
    public function getGet($key, $default);
    public function getArgs($key, $default);
    public function getUriPath();


}