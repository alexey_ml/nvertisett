<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/31/15
 * Time: 12:53 PM
 */

namespace Lib\Core\Request;

use Lib\Core\Tools\Container;

class AbstractRequest implements RequestInterface {

    /* @var Container $this->post */
    protected $post;

    /* @var Container $this->get */
    protected $get;

    /* @var Container $this->args */
    protected $args;

    /* @var string $this->uriPath */
    protected $uriPath;

    /**
     * @param Container $post
     * @return AbstractRequest
     */
    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @param Container $get
     * @return AbstractRequest
     */
    public function setGet($get)
    {
        $this->get = $get;
        return $this;
    }

    /**
     * @param Container $args
     * @return AbstractRequest
     */
    public function setArgs($args)
    {
        $this->args = $args;
        return $this;
    }

    /**
     * @param string $key
     * @param string|null $default
     * @return bool|mixed|null
     */
    public function getPost($key, $default = null)
    {
        $val = $this->post->get($key);
        if (!$val) {
            return $default;
        }

        return $val;
    }

    /**
     * @param string $key
     * @param string|null $default
     * @return bool|mixed|null
     */
    public function getGet($key, $default = null)
    {
        $val = $this->get->get($key);
        if (!$val) {
            return $default;
        }

        return $val;
    }

    /**
     * @param string $key
     * @param string|null $default
     * @return bool|mixed|null
     */
    public function getArgs($key, $default = null)
    {
        $val = $this->args->get($key);
        if (!$val) {
            return $default;
        }

        return $val;
    }

    /**
     * @return string
     */
    public function getUriPath()
    {
        return $this->uriPath;
    }

    /**
     * @param string $uriPath
     * @return AbstractRequest
     */
    public function setUriPath($uriPath)
    {
        $this->uriPath = $uriPath;
        return $this;
    }


}