<?php

namespace Lib\Core\Tools;
use Lib\Core\ServiceManager\SMResourceInterface;

/**
 * Wrapper to ArrayObject class
 * Class Container
 */
class Container extends \ArrayObject implements SMResourceInterface {

    /**
     * @param mixed $key
     * @return bool|mixed
     */
    public function get($key)
    {
        if ($this->offsetExists($key)) {
            return $this->offsetGet($key);
        }

        return false;
    }

}