<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/30/15
 * Time: 8:23 AM
 */

namespace Lib\Core\ApplicationStrategy;


class AppStrategyFactory {

    /**
     * @param string $mode
     * @return \Lib\Core\ApplicationStrategy\AppStrategyInterface;
     */
    public static function factory($mode)
    {
        $className = "\\Lib\\Core\\ApplicationStrategy\\" . $mode;
        $strategy = new $className();
        return $strategy;
    }

}