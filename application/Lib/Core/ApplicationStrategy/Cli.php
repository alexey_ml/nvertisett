<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/30/15
 * Time: 8:15 AM
 */

namespace Lib\Core\ApplicationStrategy;


use Lib\Core\Request\Request;
use Lib\Core\Request\RequestInterface;
use Lib\Core\Tools\Container;
use Lib\Core\View\CliView;

class Cli implements AppStrategyInterface {

    /**
     * @use build Request from php globals Symfony2 style
     * @return RequestInterface
     */
    public function getRequest()
    {
        $request = new Request();

        $request->setPost($this->buildPost())
            ->setGet($this->buildGet())
            ->setArgs($this->buildArgs());

        return $request;
    }

    public function getResponse(){}

    /**
     * @return CliView
     */
    public function getView()
    {
        $view = new CliView(new Container());
        return $view;
    }


    /**
     * @return Container
     */
    protected function buildPost()
    {
        $cont = new Container();

        if (isset($_POST) && !empty($_POST)) {

            foreach($_POST as $key => $val) {
                $cont->offsetSet($key, $val);
            }

        }

        return $cont;
    }

    /**
     * @return Container
     */
    protected function buildGet()
    {
        $cont = new Container();

        if (isset($_GET) && !empty($_GET)) {
            foreach($_GET as $key => $val) {
                $cont->offsetSet($key, $val);
            }

        }

        return $cont;
    }

    /**
     * @return Container
     */
    protected function buildArgs()
    {
        $cont = new Container();

        if (isset($GLOBALS['argv']) && !empty($GLOBALS['argv'])) {
            foreach($GLOBALS['argv'] as $key => $val) {
                $cont->offsetSet($key, $val);
            }

        }

        return $cont;
    }

}