<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/30/15
 * Time: 8:13 AM
 */

namespace Lib\Core\ApplicationStrategy;

use Lib\Core\ServiceManager\SMServiceInterface;

interface AppStrategyInterface extends SMServiceInterface {

    public function getRequest();
    public function getResponse();
    public function getView();

}