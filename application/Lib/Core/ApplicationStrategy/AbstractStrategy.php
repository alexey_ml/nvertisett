<?php

namespace Lib\Core\ApplicationStrategy;

use Lib\Core\Request\Request;
use Lib\Core\Request\RequestInterface;
use Lib\Core\Response\Response;
use Lib\Core\Tools\Container;
use Lib\Core\View\WebView;

class AbstractStrategy implements AppStrategyInterface {

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        $request = new Request();

        $request
            ->setPost($this->buildPost())
            ->setGet($this->buildGet())
            ->setArgs($this->buildArgs())
            ->setUriPath($_SERVER['REQUEST_URI'])
        ;

        return $request;
    }

    public function getResponse()
    {
        return new Response();
    }

    /**
     * @return WebView
     */
    public function getView()
    {
        $view = new WebView(new Container());
        return $view;
    }


    /**
     * @return Container
     */
    protected function buildPost()
    {
        $cont = new Container();

        if (isset($_POST) && !empty($_POST)) {

            foreach($_POST as $key => $val) {
                $cont->offsetSet($key, $val);
            }

        }

        return $cont;
    }

    /**
     * @return Container
     */
    protected function buildGet()
    {
        $cont = new Container();

        if (isset($_GET) && !empty($_GET)) {
            foreach($_GET as $key => $val) {
                $cont->offsetSet($key, $val);
            }

        }

        return $cont;
    }

    /**
     * @return Container
     */
    protected function buildArgs()
    {
        $cont = new Container();

        if (isset($GLOBALS['argv']) && !empty($GLOBALS['argv'])) {
            foreach($GLOBALS['argv'] as $key => $val) {
                $cont->offsetSet($key, $val);
            }

        }

        return $cont;
    }

    /**
     * @return string
     */
    protected function buildUriPath()
    {
        return $_SERVER['REQUEST_URI'];
    }

}