<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/15
 * Time: 4:07 PM
 */

namespace Lib\Core\EventManager;

use Lib\Core\Tools\Container;

abstract class Observable {

    /* @var Container $this->events */
    protected $events;

    /**
     * Array of strings - events name => array of observers
     * @param array $events
     */
    public function __construct(array $events = array())
    {
        $this->events = new Container();

        if (!empty($events)) {

            foreach($events AS $eventName => $observers) {
                $this->registerEvent($eventName, $observers);
            }
        }
    }

    /**
     * @param String $eventName
     * @param array $observers
     * @return bool
     * @throws \Exception
     */
    public function registerEvent($eventName, array $observers = array())
    {
        if(!$this->events->offsetExists($eventName)) {

            $event = new Event($eventName);

            if (!empty($observers)) {

                /* @var ObserverInterface $observer*/
                foreach($observers AS $observer) {
                    $event->attach($observer);
                }
            }

            $this->events->offsetSet($eventName, $event);

        } else {
            throw new \Exception(
                'Such event has been already registered, use update function instead to replace'
            );
        }

        return true;
    }

    /**
     * @param String $eventName
     * @return bool
     */
    public function unregisterEvent($eventName)
    {
        if($this->events->offsetExists($eventName)) {
            $this->events->offsetUnset($eventName);

        } else {
            return false;
        }

        return true;
    }

    /**
     * Unregister all Events
     */
    public function unregisterAllEvents()
    {
        $this->events = new Container();
    }

    /**
     * @param String $eventName
     * @return AbstractEvent|bool
     */
    public function getEvent($eventName)
    {
        return $this->events->get($eventName);
    }

    /**
     * @return Container
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param String $eventName
     * @return bool
     */
    public function isEventRegistered($eventName)
    {
        if($this->events->offsetExists($eventName)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $eventName
     * @return bool
     */
    abstract public function triggerEvent($eventName);
}