<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/15
 * Time: 4:15 PM
 */

namespace Lib\Core\EventManager;

use Lib\Core\ServiceManager\SMInterface;

interface ObserverInterface {

    /**
     * @param EventInterface $event
     * @param SMInterface $serviceManager
     */
    public function fireByEvent($event, $serviceManager);

}