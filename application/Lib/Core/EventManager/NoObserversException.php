<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/15
 * Time: 4:57 PM
 */

namespace Lib\Core\EventManager;

use Lib\Core\Exception\LogicException;

class _NoObserversException extends LogicException {

}