<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/15
 * Time: 4:10 PM
 */

namespace Lib\Core\EventManager;

use Lib\Core\ServiceManager\SMInterface;
use Lib\Core\Tools\Container;

abstract class AbstractEvent implements EventInterface {

    /* @var \ArrayObject $this->observers */
    protected $observers;

    /* @var string $this->name */
    protected $name;

    /**
     * @param string $name
     * Array of observers
     * @param array $observers
     */
    public function __construct($name, array $observers = array())
    {
        $this->name = $name;
        $this->observers = new Container();

        if (!empty($observers)) {

            /* @var ObserverInterface $observer */
            foreach($observers AS $observer) {
                $this->attach($observer);

            }
        }
    }

    /**
     * @param ObserverInterface $observer
     * @return EventInterface
     */
    public function attach(ObserverInterface $observer)
    {
        $key = $this->getKey($observer);
        $this->observers->offsetSet($key, $observer);

        return $this;
    }

    /**
     * @param ObserverInterface $observer
     * @return EventInterface
     */
    public function detach(ObserverInterface $observer)
    {
        $key = $this->getKey($observer);
        if ($this->observers->offsetExists($key)) {
            $this->observers->offsetUnset($key);
        }

        return $this;
    }

    /**
     * Detach all the observers
     */
    public function detachAll()
    {
        $this->observers = new Container();
    }

    /**
     * @param SMInterface $sm
     * @return bool
     */
    public function trigger($sm = null)
    {
        /* @var int $count */
        $count = (int)$this->observers->count();
        if ($count > 0) {

            foreach ($this->observers AS $observer) {
                $observer->fireByEvent($this, $sm);
            }

        } else {
            return false;
        }

        return true;

    }

    /**
     * @return Container
     */
    public function getObservers()
    {
        return $this->observers;
    }

    /**
     * @param string $key
     * @return Container|null
     */
    public function getObserver($key)
    {
        if ($this->observers->offsetExists($key)) {
            return $this->observers->offsetGet($key);
        }

        return null;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function isAttached($key)
    {
        if ($this->observers->offsetExists($key)) {
            return true;
        }

        return false;
    }

    /**
     * @param ObserverInterface $observer
     * @return string
     */
    public function getKey(ObserverInterface $observer)
    {
        return spl_object_hash($observer);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}