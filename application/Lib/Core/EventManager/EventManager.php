<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/12/15
 * Time: 6:05 PM
 */

namespace Lib\Core\EventManager;

use Lib\Core\ServiceManager\SMInterface;
use Lib\Core\ServiceManager\SMServiceInterface;

class EventManager extends Observable  implements SMServiceInterface {

    /* @var SMInterface $this->sm */
    protected $serviceManager;

    /**
     * @param String $eventName
     * @return bool
     * @throws EventNotFoundException
     */
    public function triggerEvent($eventName)
    {
        if ($this->events->offsetExists($eventName)) {

            /* @var AbstractEvent $event */
            $event = $this->events->get($eventName);
            $event->trigger($this->serviceManager);

        } else {
            throw new EventNotFoundException('Unknown Event');

        }

        return true;
    }

    /**
     * @return SMInterface
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @param SMInterface $sm
     */
    public function setServiceManager(SMInterface $sm)
    {
        $this->serviceManager = $sm;
    }



}