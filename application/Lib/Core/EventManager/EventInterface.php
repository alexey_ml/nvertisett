<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/30/15
 * Time: 9:21 PM
 */

namespace Lib\Core\EventManager;
use Lib\Core\ServiceManager\SMInterface;

interface EventInterface {

    /**
     * @param SMInterface|null $sm
     * @return bool
     */
    public function trigger($sm = null);
}