<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/30/15
 * Time: 8:39 AM
 */

namespace Lib\Core\Dispatcher;

use Lib\Core\EventManager\EventManager;

interface DispatcherInterface {
    /**
     * @param EventManager $eventManager
     * @return bool
     */
    public function dispatch($eventManager);

    /**
     * @param string $hookName
     * @param string $eventName
     * @return void
     */
    public function addEventToHook($hookName, $eventName);
}