<?php

namespace Lib\Core\Dispatcher;

use Lib\Core\EventManager\EventManager;
use Lib\Core\EventManager\EventNotFoundException;
use Lib\Core\Tools\Container;
use Lib\Core\ServiceManager\SMServiceInterface;


class Dispatcher implements DispatcherInterface, SMServiceInterface {

    /* @var Container $this->hooks */
    protected $hooks;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->hooks = new Container();

        foreach($config AS $order => $hook) {

            $this->hooks->offsetSet(
                $hook, array()
            );

        }
    }

    /**
     * @param string $hookName
     * @param string $eventName
     */
    public function addEventToHook($hookName, $eventName)
    {
        /* @var array $events */
        $events = $this->hooks->get($hookName);
        $events[] = $eventName;
        $this->hooks->offsetSet($hookName, $events);
    }

    /**
     * @param EventManager $eventManager
     * @return bool
     * @throws EventNotFoundException
     */
    public function dispatch($eventManager)
    {
        if (empty($this->hooks)) {
            return false;
        }

        foreach($this->hooks AS $hook => $events) {
            if (!empty($events)) {
                foreach($events AS $event) {
                    $eventManager->triggerEvent($event);

                }
            }
        }

        return true;
    }

}