<?php
namespace Lib\Core\Application;

use Lib\Core\ApplicationStrategy\AppStrategyFactory;
use Lib\Core\ApplicationStrategy\AppStrategyInterface;
use Lib\Core\ApplicationStrategy\Http;
use Lib\Core\Bootstrap\AbstractBootstrap;
use Lib\Core\Bootstrap\ModuleBootstrapFactory;
use Lib\Core\Controller\ControllerFactory;
use Lib\Core\Dispatcher\Dispatcher;
use Lib\Core\EventManager\EventManager;
use Lib\Core\Router\MatchedRoute;
use Lib\Core\Router\Router;
use Lib\Core\ServiceManager\ServiceManager;
use Lib\Core\Tools\Container;
use Lib\Core\Tools\MainConfig;
use Lib\Vendor\PHPPDOWrapper\DB as PDOWrapper;

class Application implements ApplicationInterface {

    /* @var ServiceManager $this->sm */
    protected $serviceManager;

    /**
     * @use Add the essential services and resources here
     * @param string $mode
     * @param AbstractBootstrap $bootstrap
     * @param array $config
     */
    public function __construct($mode, $bootstrap, $config)
    {

    // Set Service Manager
        $this->serviceManager = new ServiceManager(
            new Container(), new Container()
        );

    // Add config resource to sm
        $this->serviceManager->addResource('mainConfig', new MainConfig($config));

    // Add Bootstrap to ServiceManager
        $this->serviceManager->addService('bootstrap', $bootstrap);

    // Add Application Strategy to ServiceManager
        $this->serviceManager->addService('appStrategy', AppStrategyFactory::factory($mode));

    // Add Router to ServiceManager
        $router = new Router();
        $this->serviceManager->addService('router', $router);

    // Add Dispatcher to ServiceManager
        $this->serviceManager->addService('dispatcher',
            new Dispatcher($this->serviceManager->getResource('mainConfig')['dispatcher'])
        );

    // Add Event Manager
        $eventManager = new EventManager();
        $eventManager->setServiceManager($this->serviceManager);
        $this->serviceManager->addService('eventManager', $eventManager);

        /**
         * Boot Up MySQL class: @url http://www.imavex.com/php-pdo-wrapper-class
         */

        /* @var Container $conf */
        $conf = $this->serviceManager->getResource('mainConfig');
        $dbConf = $conf->get('db');
        $db = new PDOWrapper($dbConf['dsn'], $dbConf['username'], $dbConf['password']);
        $db->run('SET NAMES utf8');
        $this->serviceManager->addService('db', $db);

    }

    /**
     * Boot resources and services and fill in Events and Dispatched
     */
    public function boot()
    {
        /* @var EventManager $eMngr */
        $eMngr = $this->serviceManager->getService('eventManager');

        /* @var Dispatcher $dispatcher */
        $dispatcher = $this->serviceManager->getService('dispatcher');

        // Bind the bootstrap Event
        $eventName = 'onBoostrap';
        $eMngr->registerEvent($eventName, array(
            $this->serviceManager->getService('bootstrap')
        ));
        $dispatcher->addEventToHook('bootstrap', $eventName);
//        $eMngr->triggerEvent($eventName);

        /**
         * Register onRoute event and listeners
         * MVC event will be bind in Router too after of MatchedRoute resolving
         */
        $eventName = 'onRoute';
        $eMngr->registerEvent($eventName, array(
            $this->serviceManager->getService('router')
        ));
//        $eMngr->triggerEvent('onRoute');
        $dispatcher->addEventToHook('route', $eventName);

        return $this;

    }

    /**
     * @use Run Dispatcher
     * @return bool
     * @throws \Exception
     */
    public function run()
    {
        /* @var EventManager $eMngr */
        $eMngr = $this->serviceManager->getService('eventManager');

        /* @var Dispatcher $dispatcher */
        $dispatcher = $this->serviceManager->getService('dispatcher');

        return $dispatcher->dispatch($eMngr);

    }

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }



}