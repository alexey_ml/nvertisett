<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:20 PM
 */

namespace Lib\Core\Application;


use Lib\Core\ServiceManager\SMInterface;

interface ApplicationInterface {

    const MODECLI = 'Cli';
    const MODEHTTP = 'Http';
    const MODEAUTO = 'Auto';

    /**
     * @return ApplicationInterface
     */
    public function boot();

    /**
     * @return bool
     * @throws \Exception
     */
    public function run();

    /**
     * @return SMInterface
     */
    public function getServiceManager();


}