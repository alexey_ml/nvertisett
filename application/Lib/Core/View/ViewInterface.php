<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 11/1/15
 * Time: 7:22 PM
 */

namespace Lib\Core\View;

use Lib\Core\ServiceManager\SMServiceInterface;

interface ViewInterface extends SMServiceInterface{

    /**
     * @param string $key
     * @param mixed $val
     * @return void
     */
    public function assign($key, $val);

    /**
     * @param string $script
     * @return mixed
     */
    public function render($script);

}