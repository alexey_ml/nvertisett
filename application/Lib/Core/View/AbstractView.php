<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 11/1/15
 * Time: 7:24 PM
 */

namespace Lib\Core\View;


use Lib\Core\Tools\Container;

class AbstractView implements ViewInterface {

    /* @var Container $this->data */
    protected $data;

    /**
     * @param Container $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param string $key
     * @param mixed $val
     */
    public function assign($key, $val)
    {
        $this->data->offsetSet($key, $val);
    }

    /**
     * @return Container
     */
    public function getVars()
    {
        return $this->data;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getVar($key)
    {
        return $this->data->get($key);
    }

    /**
     * @param string $script
     * @return mixed
     */
    public function render($script)
    {
//        extract($this->data);
        ob_start();
        include($script);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

}