<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 11/1/15
 * Time: 12:40 PM
 */

namespace Lib\Core\Model;
use Lib\Core\ServiceManager\ServiceManager;

/**
 * Class AbstractModel
 * @package Lib\Core\Model
 * @use simple model with some bussiness logic
 */
class AbstractModel implements ModelInterface {

    /* @var ServiceManager $this->serviceManager */
    protected $serviceManager;

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

}