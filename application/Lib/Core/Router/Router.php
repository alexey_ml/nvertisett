<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 16.06.16
 * Time: 16:16
 */

namespace Lib\Core\Router;


use Lib\Core\Bootstrap\ModuleBootstrapFactory;
use Lib\Core\Controller\ControllerFactory;
use Lib\Core\Dispatcher\Dispatcher;
use Lib\Core\EventManager\EventManager;
use Lib\Core\EventManager\ObserverInterface;
use Lib\Core\EventManager\EventInterface;
use Lib\Core\Request\Request;
use Lib\Core\ServiceManager\ServiceManager;
use Lib\Core\ServiceManager\SMResourceInterface;

class Router implements ObserverInterface, RouterInterface
{
    /**
     * @param EventInterface $event
     * @param ServiceManager $serviceManager
     */
    public function fireByEvent($event, $serviceManager)
    {
        $config = $serviceManager->getResource('mainConfig');
        $mRoute = $this->resolveRoute(
            $serviceManager->getResource('request'),
            $config['routes']
        );

//        $mRoute = new MatchedRoute();
//        $mRoute->setModuleName('Homepage')->setControllerName('Index')->setActionName('Index');
        $serviceManager->addResource('matchedRoute', $mRoute);

        $controller = ControllerFactory::factory($serviceManager);
        $controller->setServiceManager($serviceManager);

        // Boot module-specific resources and services
        $modBoot = ModuleBootstrapFactory::factory($serviceManager);

        $serviceManager->addService('matchedController', $controller);
        $serviceManager->addService('moduleBootstrap', $modBoot);

    // Build the controller and set the onMvc event here

        /* @var EventManager $eMngr */
        $eMngr = $serviceManager->getService('eventManager');

        /* @var Dispatcher $dispatcher */
        $dispatcher = $serviceManager->getService('dispatcher');

        $eventName = 'onMvc';
        $eMngr->registerEvent($eventName, array(
            $serviceManager->getService('moduleBootstrap'),
            $serviceManager->getService('matchedController')
        ));
        $dispatcher->addEventToHook('mvc', $eventName);

    }

    /**
     * Simple Route resolver based on RequestURI
     *
     * @param SMResourceInterface|Request $request
     * @param array $routes
     * @return MatchedRoute
     */
    public function resolveRoute($request, $routes)
    {
        $mRoute = new MatchedRoute();

        $path = $request->getUriPath();
        foreach($routes AS $key => $route) {
            if ($path == $route['route']) {
                return $mRoute
                    ->setRouteName($key)
                    ->setModuleName($route['module'])
                    ->setControllerName($route['controller'])
                    ->setActionName($route['action']);
            }
        }

        /**
         * Redirect to 404 page
         */
        return $mRoute
            ->setRouteName('page_not_found')
            ->setModuleName($routes['page_not_found']['module'])
            ->setControllerName($routes['page_not_found']['controller'])
            ->setActionName($routes['page_not_found']['action']);

    }
}