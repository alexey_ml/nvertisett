<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 16.06.16
 * Time: 18:08
 */

namespace Lib\Core\Router;


use Lib\Core\Request\Request;
use Lib\Core\ServiceManager\SMResourceInterface;
use Lib\Core\ServiceManager\SMServiceInterface;

interface RouterInterface extends SMServiceInterface
{
    /**
     * @param SMResourceInterface|Request $request
     * @param array $routes
     * @return MatchedRoute
     */
    public function resolveRoute($request, $routes);
}