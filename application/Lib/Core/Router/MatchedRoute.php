<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/31/15
 * Time: 4:26 PM
 */

/**
 * Class MatchedRoute
 * @use just a dummy, lest hardcore data to run it for the test application
 */

namespace Lib\Core\Router;

use Lib\Core\ServiceManager\SMResourceInterface;

class MatchedRoute implements SMResourceInterface {

    /* @var string $this->moduleName */
    protected $moduleName;

    /* @var string $this->controllerName */
    protected $controllerName;

    /* @var string $this->actionName */
    protected $actionName;

    /* @var string $this->routeName */
    protected $routeName;

    /**
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * @param string $moduleName
     * @return MatchedRoute
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;
        return $this;
    }

    /**
     * @return string
     */
    public function getControllerName()
    {
        return $this->controllerName;
    }

    /**
     * @param string $controllerName
     * @return MatchedRoute
     *
     */
    public function setControllerName($controllerName)
    {
        $this->controllerName = $controllerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * @param string $actionName
     * @return MatchedRoute
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * @param mixed $routeName
     * @return MatchedRoute
     */
    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;
        return $this;
    }




}