<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 11/1/15
 * Time: 12:29 PM
 */

namespace Lib\Core\Bootstrap;

use Lib\Core\Router\MatchedRoute;
use Lib\Core\ServiceManager\ServiceManager;

class ModuleBootstrapFactory {

    /**
     * @param ServiceManager $serviceManager
     * @return AbstractBootstrap
     */
    public static function factory($serviceManager)
    {
        /* @var MatchedRoute $mRoute */
        $mRoute = $serviceManager->getResource('matchedRoute');
        $modBootName = '\\Module\\' . $mRoute->getModuleName() . '\\Module';
        $modBoot = new $modBootName();

        return $modBoot;
    }
}