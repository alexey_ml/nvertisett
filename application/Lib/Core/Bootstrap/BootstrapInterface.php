<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:25 PM
 */

namespace Lib\Core\Bootstrap;

use Lib\Core\EventManager\EventInterface;
use Lib\Core\EventManager\ObserverInterface;
use Lib\Core\ServiceManager\SMInterface;

interface BootstrapInterface extends ObserverInterface {

    /**
     * @param EventInterface $event
     * @return void
     */
    public function boot($event);

    /**
     * @param EventInterface $event
     * @param SMInterface $serviceManager
     * @return void
     */
    public function fireByEvent($event, $serviceManager);


}