<?php

namespace Lib\Core\Bootstrap;

use Lib\Core\EventManager\EventInterface;
use Lib\Core\ApplicationStrategy\AppStrategyInterface;
use Lib\Core\Router\Router;

class CoreBootstrap extends AbstractBootstrap {

    /**
     * @param EventInterface $event
     * @return void
     */
    public function boot($event)
    {
        $this->buildRequest();
        $this->buildResponse();
        $this->buildView();
    }

    public function buildRequest()
    {
        /* @var AppStrategyInterface $appStrategy */
        $appStrategy = $this->serviceManager->getService('appStrategy');
        $request = $appStrategy->getRequest();
        $this->serviceManager->addResource('request', $request);
    }

    public function buildResponse()
    {
        /* @var AppStrategyInterface $appStrategy */
        $appStrategy = $this->serviceManager->getService('appStrategy');
        $response = $appStrategy->getResponse();
        $this->serviceManager->addResource('response', $response);
    }

    public function buildView()
    {
        /* @var AppStrategyInterface $appStrategy */
        $appStrategy = $this->serviceManager->getService('appStrategy');
        $view = $appStrategy->getView();
        $this->serviceManager->addService('view', $view);
    }



}