<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/31/15
 * Time: 10:52 AM
 */

namespace Lib\Core\Bootstrap;

use Lib\Core\EventManager\EventInterface;
use Lib\Core\ServiceManager\ServiceManager;
use Lib\Core\ServiceManager\SMInterface;
use Lib\Core\ServiceManager\SMServiceInterface;

abstract class AbstractBootstrap implements BootstrapInterface, SMServiceInterface{

    /* @var ServiceManager $this->sm */
    protected $serviceManager;

    /**
     * @param EventInterface $event
     * @param SMInterface $serviceManager
     * @return void
     */
    public final function fireByEvent($event, $serviceManager)
    {
        $this->serviceManager= $serviceManager;
        $this->boot($event);
    }

    /**
     * @param EventInterface $event
     * @return void
     */
    abstract public function boot($event);

}