<?php

use \Lib\Core\Router\Router;
use \Lib\Core\Request\Request;


/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class RouterTest extends \PHPUnit_Framework_TestCase
{
    protected static $config;

    public static function setUpBeforeClass()
    {
        $realpath = realpath(dirname(__FILE__));
        !defined('APP_PATH') && define('APP_PATH', $realpath . '/../../../../../');
        !defined('DS') && define('DS', DIRECTORY_SEPARATOR);

        if (!function_exists('error_to_exception')) {
            include(APP_PATH . '/config/err_handler.php');
        }
        include(APP_PATH . '/config/autoloader.php');
        static::$config = (array)include(APP_PATH . '/config/config.php');
    }

    /**
     * Test for resolving of Existing Routes
     */
    public function testRouterForExistingRoutes()
    {
        /* @var Router $router */
        $router = new Router();

        $request = new Request();
        $routes = static::$config['routes'];

        $request->setUriPath('/');
        $matchedRoute = $router->resolveRoute($request, $routes);
        $this->assertEquals($matchedRoute->getRouteName(), 'home');
        $this->assertEquals($matchedRoute->getModuleName(), 'Homepage');
        $this->assertEquals($matchedRoute->getControllerName(), 'Index');
        $this->assertEquals($matchedRoute->getActionName(), 'Index');

        $request->setUriPath('/api/comments');
        $matchedRoute = $router->resolveRoute($request, $routes);
        $this->assertEquals($matchedRoute->getRouteName(), 'comments');
        $this->assertEquals($matchedRoute->getModuleName(), 'Article');
        $this->assertEquals($matchedRoute->getControllerName(), 'Comments');
        $this->assertEquals($matchedRoute->getActionName(), 'Comments');

        $request->setUriPath('/api/comment/add');
        $matchedRoute = $router->resolveRoute($request, $routes);
        $this->assertEquals($matchedRoute->getRouteName(), 'comment_add');
        $this->assertEquals($matchedRoute->getModuleName(), 'Article');
        $this->assertEquals($matchedRoute->getControllerName(), 'Comments');
        $this->assertEquals($matchedRoute->getActionName(), 'Save');

    }

    /**
     * Test for resolving of Non-Existing Routes
     * Should return 404 wrapper
     */
    public function testRouterForNonExistingRoutes()
    {
        /* @var Router $router */
        $router = new Router();

        $request = new Request();
        $routes = static::$config['routes'];

        $request->setUriPath('/somewhere/out/here');
        $matchedRoute = $router->resolveRoute($request, $routes);
        $this->assertEquals($matchedRoute->getRouteName(), 'page_not_found');
        $this->assertEquals($matchedRoute->getModuleName(), 'Errors');
        $this->assertEquals($matchedRoute->getControllerName(), 'NotFound');
        $this->assertEquals($matchedRoute->getActionName(), 'Index');
    }


}