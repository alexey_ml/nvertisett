<?php

use \Lib\Core\EventManager\EventManager;
use \Module\Test\Observer\Bar;
use \Module\Test\Observer\Foo;
use \Lib\Core\EventManager\EventNotFoundException;


/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class EventManagerTest  extends \PHPUnit_Framework_TestCase
{

    public static function setUpBeforeClass()
    {
        $realpath = realpath(dirname(__FILE__));
        !defined('APP_PATH') && define('APP_PATH', $realpath . '/../../../../../');
        !defined('DS') && define('DS', DIRECTORY_SEPARATOR);

        if (!function_exists('error_to_exception')) {
            include(APP_PATH . '/config/err_handler.php');
        }
        include(APP_PATH . '/config/autoloader.php');
    }

    /**
     * Test for registering and triggering of existing Event/Listener
     * 1. Create new EventManager
     * 2. Register fixtured Listener
     * 3. Fire Event
     * 4. Write and test output
     *
     * @throws EventNotFoundException
     * @throws Exception
     */
    public function testExistingEvent()
    {
        /* @var EventManager $em */
        $em = new EventManager([]);

        /**
         * Bar Event
         */
        $em->registerEvent('testEventOne', array(
            new Bar()
        ));

        ob_start();
        $em->triggerEvent('testEventOne');
        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEquals($output, 'Hello From Bar Observer');


        /**
         * Foo Event
         */
        $em->registerEvent('testEventTwo', array(
            new Foo()
        ));

        ob_start();
        $em->triggerEvent('testEventTwo');
        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEquals($output, 'Hi From Foo Observer');

        /**
         * Foo and Bar Events together
         */
        $em->registerEvent('testEventThree', array(
            new Foo(), new Bar()
        ));

        ob_start();
        $em->triggerEvent('testEventThree');
        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEquals($output, 'Hi From Foo ObserverHello From Bar Observer');

        /**
         * and run Bar Event again after adding of new Events/Subs
         */

        ob_start();
        $em->triggerEvent('testEventOne');
        $output = ob_get_contents();
        ob_end_clean();

        $this->assertEquals($output, 'Hello From Bar Observer');

    }

    /**
     * Test for firing of non-existing Event
     * @throws EventNotFoundException
     */
    public function testNonExistingEvent()
    {
        /* @var EventManager $em */
        $em = new EventManager([]);

        try {
            $em->triggerEvent('NonExistingOneEvent');

        } catch(EventNotFoundException $e) {
            $this->assertTrue($e instanceOf EventNotFoundException);
        }
    }

    /**
     * Event with the same name can't be registered again
     * @throws Exception
     */
    public function testRegisterEventWithTheSameNameAgain()
    {
        /* @var EventManager $em */
        $em = new EventManager([]);

        $em->registerEvent('testEventOne', array(
            new Bar()
        ));

        try {
            $em->registerEvent('testEventOne', array(
                new Bar()
            ));

        } catch(\Exception $e) {
            $this->assertEquals(
                $e->getMessage(),
                'Such event has been already registered, use update function instead to replace'
            );
        }
    }

    /**
     * Test isEventRegistered foo
     * @throws Exception
     */
    public function testIsEventRegistered()
    {
        /* @var EventManager $em */
        $em = new EventManager([]);

        $em->registerEvent('testEventOne', array(
            new Bar())
        );

        $em->registerEvent('testEventTwo', array(
            new Foo()
        ));

        $this->assertTrue($em->isEventRegistered('testEventOne'));
        $this->assertTrue($em->isEventRegistered('testEventTwo'));
        $this->assertFalse($em->isEventRegistered('nonExistingEvent'));
    }

    /**
     * Test unregisterEvent foo
     * @throws Exception
     */
    public function testUnregisterEvent()
    {
        /* @var EventManager $em */
        $em = new EventManager([]);

        $em->registerEvent('testEventOne', array(
            new Bar())
        );

        $em->registerEvent('testEventTwo', array(
            new Foo()
        ));

        $em->unregisterEvent('testEventTwo');

        $this->assertTrue($em->isEventRegistered('testEventOne'));
        $this->assertFalse($em->isEventRegistered('testEventTwo'));

    }

    /**
     * Test unregisterAllEvents foo
     * @throws Exception
     */
    public function testUnregisterAllEvents()
    {
        /* @var EventManager $em */
        $em = new EventManager([]);

        $em->registerEvent('testEventOne', array(
            new Bar())
        );

        $em->registerEvent('testEventTwo', array(
            new Foo()
        ));

        $em->unregisterAllEvents();
        $this->assertFalse($em->isEventRegistered('testEventOne'));
        $this->assertFalse($em->isEventRegistered('testEventTwo'));
    }


}