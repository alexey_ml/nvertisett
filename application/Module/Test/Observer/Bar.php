<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 18.06.16
 * Time: 16:45
 */

namespace Module\Test\Observer;


use Lib\Core\EventManager\EventInterface;
use Lib\Core\EventManager\ObserverInterface;
use Lib\Core\ServiceManager\SMInterface;

class Bar implements ObserverInterface
{
    /**
     * @param EventInterface $event
     * @param SMInterface $serviceManager
     */
    public function fireByEvent($event, $serviceManager)
    {
        echo 'Hello From Bar Observer';
    }
}