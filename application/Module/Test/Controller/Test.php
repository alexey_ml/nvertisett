<?php

namespace Module\Test\Controller;

use Lib\Core\Controller\WebController;
use Lib\Core\EventManager\EventManager;


class Test extends WebController {

    /**
     * Test Observers
     */
    public function IndexAction()
    {
        /* @var EventManager $em */
        $em = $this->serviceManager->getService('eventManager');
        $em->triggerEvent('testEventOne');
        $em->triggerEvent('testEventTwo');
    }

}