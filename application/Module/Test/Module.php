<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:26 PM
 */

namespace Module\Test;

use Lib\Core\Bootstrap\AbstractBootstrap;
use Lib\Core\EventManager\EventInterface;
use Lib\Core\EventManager\EventManager;
use Lib\Core\ServiceManager\ServiceManager;
use Lib\Core\ServiceManager\SMServiceInterface;
use Lib\Vendor\PHPPDOWrapper\DB;


/**
 * @use Boot up the Module-specific resources
 */
class Module extends AbstractBootstrap {

    /**
     * @param EventInterface $event
     * @return void
     */
    public function boot($event)
    {
        $this->registerObservers();
    }

    protected function registerObservers()
    {
        /* @var DB $db */
        $db = $this->serviceManager->getService('db');
        $res = $db->select('observer');

        /* @var EventManager $em */
        $em = $this->serviceManager->getService('eventManager');
        foreach($res AS $suite) {
            $em->registerEvent($suite['event_name'], unserialize(base64_decode($suite['observers'])));
        }
    }


}