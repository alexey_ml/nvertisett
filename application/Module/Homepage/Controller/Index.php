<?php

namespace Module\Homepage\Controller;

use Lib\Core\Controller\WebController;
use Lib\Core\Response\Response;
use Lib\Core\Response\ResponseInterface;
use Lib\Core\View\WebView;
use Module\Article\Model\Article;

class Index extends WebController {

    public function IndexAction()
    {
        /* @var WebView $view */
        $view = $this->serviceManager->getService('view');

        /* @var Article $articleModel */
        $articleModel = $this->getServiceManager()->getService('articleModel');
        $view->assign('article', $articleModel->getById(1));

        $html = $view->render(APP_PATH . '/Module/Homepage/View/index.phtml');

        /* @var Response $response*/
        $response = $this->getServiceManager()->getResource('response');
        $response
            ->setContentType(ResponseInterface::CONTTYPE_HTML)
            ->setContent($html)
            ->setStatus(ResponseInterface::RESPONSE_OK);

        $this->sendResponse($response);
    }

}