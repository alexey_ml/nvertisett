<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:26 PM
 */

namespace Module\Homepage;

use Lib\Core\Bootstrap\AbstractBootstrap;
use Lib\Core\EventManager\EventInterface;
use Lib\Core\ServiceManager\ServiceManager;
use Lib\Core\ServiceManager\SMServiceInterface;
use Module\Article\Model\Article;

/**
 * @use Boot up the Module-specific resources
 */
class Module extends AbstractBootstrap {

    /**
     * @param EventInterface $event
     * @return void
     */
    public function boot($event)
    {
        $this->serviceManager->addService(
            'articleModel', $this->bootArticleModel($this->serviceManager)
        );
    }

    /**
     * @param ServiceManager $sm
     * @return SMServiceInterface
     */
    protected function bootArticleModel($sm)
    {
        $model = new Article();
        $model->setServiceManager($sm);
        return $model;
    }

}