<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:26 PM
 */

namespace Module\Errors;

use Lib\Core\Bootstrap\AbstractBootstrap;
use Lib\Core\EventManager\EventInterface;

/**
 * @use Boot up the Module-specific resources
 */
class Module extends AbstractBootstrap {

    /**
     * @param EventInterface $event
     * @return void
     */
    public function boot($event)
    {
    }

}