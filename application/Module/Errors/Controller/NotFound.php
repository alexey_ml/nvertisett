<?php

namespace Module\Errors\Controller;

use Lib\Core\Controller\WebController;
use Lib\Core\Response\Response;
use Lib\Core\Response\ResponseInterface;
use Lib\Core\View\WebView;

class NotFound extends WebController {

    public function IndexAction()
    {
        /* @var WebView $view */
        $view = $this->serviceManager->getService('view');
        $html = $view->render(APP_PATH . '/Module/Errors/View/page_not_found.phtml');

        /* @var Response $response*/
        $response = $this->getServiceManager()->getResource('response');
        $response
            ->setContentType(ResponseInterface::CONTTYPE_HTML)
            ->setContent($html)
            ->setStatus(ResponseInterface::RESPONSE_NOTFOUND);

        $this->sendResponse($response);
    }

}