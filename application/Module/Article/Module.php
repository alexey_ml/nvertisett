<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/29/15
 * Time: 7:26 PM
 */

namespace Module\Article;

use Lib\Core\Bootstrap\AbstractBootstrap;
use Lib\Core\EventManager\EventInterface;
use Lib\Core\ServiceManager\ServiceManager;
use Lib\Core\ServiceManager\SMServiceInterface;
use Module\Article\Model\Comment;

/**
 * @use Boot up the Module-specific resources
 */
class Module extends AbstractBootstrap {

    /**
     * @param EventInterface $event
     * @return void
     */
    public function boot($event)
    {
        $this->serviceManager->addService(
            'commentModel', $this->bootCommentModel($this->serviceManager)
        );
    }

    /**
     * @param ServiceManager $sm
     * @return SMServiceInterface
     */
    protected function bootCommentModel($sm)
    {
        $model = new Comment();
        $model->setServiceManager($sm);
        return $model;
    }


}