<?php

namespace Module\Article\Controller;

use Lib\Core\Controller\WebController;
use Lib\Core\Request\Request;
use Lib\Core\Response\Response;
use Lib\Core\Response\ResponseInterface;
use Module\Article\Model\Comment;

class Comments extends WebController {

    /**
     * Comments List
     * @use backbone/REST fetch() method
     */
    public function CommentsAction()
    {
        /* @var Comment $commentModel */
        $commentModel = $this->getServiceManager()->getService('commentModel');
        $res = $commentModel->getByArticleId(1);
        $content = json_encode($res);

        /* @var Response $response*/
        $response = $this->getServiceManager()->getResource('response');
        $response
            ->setContentType(ResponseInterface::CONTTYPE_JSON)
            ->setContent($content)
            ->setStatus(ResponseInterface::RESPONSE_OK);

        $this->sendResponse($response);

    }

    /**
     * Save new Comment
     */
    public function SaveAction()
    {
        /* @var Request $request */
        $request = $this->getServiceManager()->getResource('request');

        /* @var Comment $commentModel */
        $commentModel = $this->getServiceManager()->getService('commentModel');
        $data = json_decode($request->getPost('model'), true);
        $res = $commentModel->add($data);

        /* @var Response $response*/
        $response = $this->getServiceManager()->getResource('response');
        $response->setContentType(ResponseInterface::CONTTYPE_JSON);

        if ($res) {
            $response
                ->setStatus(ResponseInterface::RESPONSE_OK)
                ->setContent('{"result": "success"}');
            goto send;
        }

        $response
            ->setStatus(ResponseInterface::RESPONSE_SERVERROR)
            ->setContent('{"result": "error"}');

        send:
            $this->sendResponse($response);
    }

}