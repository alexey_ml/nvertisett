<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 18.06.16
 * Time: 12:51
 */

namespace Module\Article\Model;


use Lib\Core\Model\AbstractModel;
use Lib\Vendor\PHPPDOWrapper\DB;

class Comment  extends AbstractModel
{
    /**
     * @param int $id
     * @return array|bool|int
     */
    public function getByArticleId($id)
    {
        /* @var DB $db */
        $db = $this->getServiceManager()->getService('db');
        return $db->select('comment', "article_id=" . (int)$id);
    }

    public function add($data)
    {
        /* @var DB $db */
        $db = $this->getServiceManager()->getService('db');

        $insert = array(
            'article_id' => (int)$data['articleId'],
            'title' => $data['title'],
            'content' => $data['content'],
            'author_name' => $data['author']
        );

        return $db->insert("comment", $insert);
    }

}