<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 18.06.16
 * Time: 12:36
 */

namespace Module\Article\Model;

use \Lib\Core\Model\AbstractModel;
use Lib\Vendor\PHPPDOWrapper\DB;


class Article extends AbstractModel
{
    /**
     * @param int $id
     * @return array|bool|int
     */
    public function getById($id)
    {
        /* @var DB $db */
        $db = $this->getServiceManager()->getService('db');
        return $db->select('article', "id=" . (int)$id);
    }

    public function increaseCommentsNum($id)
    {

    }
}