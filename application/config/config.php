<?php

return array(

// Dispatcher hooks
    'dispatcher' => array(
        1 => 'bootstrap', // bootstrap the Application
        2 => 'route',     // calc matchedRoute
        3 => 'mvc',       // run controller and action
        4 => 'response'   // send response
    ),

// Routes
    'routes' => array(

        'home' => array(
            'route' => '/',
            'module' => 'Homepage',
            'controller' => 'Index',
            'action' => 'Index'
        ),

        'comments' => array(
            'route' => '/api/comments',
            'module' => 'Article',
            'controller' => 'Comments',
            'action' => 'Comments',
            'method' => 'GET'
        ),

        'comment_add' => array(
            'route' => '/api/comment/add',
            'module' => 'Article',
            'controller' => 'Comments',
            'action' => 'Save',
            'method' => 'POST'
        ),

        'test_observers' => array(
            'route' => '/test/observers',
            'module' => 'Test',
            'controller' => 'Test',
            'action' => 'Index',
            'method' => 'GET'
        ),

        'page_not_found' => array(
            'route' => '/error/notfound',
            'module' => 'Errors',
            'controller' => 'NotFound',
            'action' => 'Index',
            'method' => 'GET'
        )

    ),

// Database
    'db' => array(
        'type' => 'mysql',
        'host' => '127.0.0.1',
        'port' => '',
        'dbname' => 'nvertisett',
        'username' => 'root',
        'password' => 'J8uv01Bq431j',
        'dsn' => 'mysql:host=127.0.0.1;dbname=nvertisett'
    ),

);