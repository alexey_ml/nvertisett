<?php
/**
 * Created by PhpStorm.
 * User: alex
 */

/**
 * Inspire to write perfect code. everything is an exception, even minor warnings.
 **/

/**
 * @param int $errno
 * @param string $errstr
 * @param string $errfile
 * @param int $errline
 * @throws ErrorException
 */
function error_to_exception($errno, $errstr, $errfile, $errline)
{
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
}

set_error_handler('error_to_exception');