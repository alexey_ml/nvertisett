# README #
## About this application ##

This is a test application, written by me from the scratch, this is not an existing framework nor a part of any framework, its my own development.
This applications is a simple event-driven MVC application.

Main components of Application are: Application and Application Strategy, Service Manager, Event Manager and Dispatcher, Modules, Controllers and Actions, and Models.
Components have self-descriptive names and located in: 

* **application/Lib/Core**

Other typical references:
    
*     Modules, Controllers and other business components: **application/Module**
*     3rd part components:** application/Lib/Vendor**
*     Tests: **application/Tests**

everything is done in the **master** branch (git) and you can review it right here in the [Source](https://bitbucket.org/alexey_ml/nvertisett/src)  part.

## Brief flow description ##

input point for the requests processing is index.php which is a FrontController, and related .htaccess file.

* References: **public/index.php**, **public/.htaccess**

Autoloader, error handler and config are loading from the file system here, and here the Application main object is being built. 

* References: **application/config/autoloader.php**, **application/config/config.php**, **application/config/err_handler.php**

While  building, the essential components of Application are booting up. After that, another boot step is being performed - rest of the services and resources, like database, are booting, and everything is stored in the Service Manager, which is the main object pool accessible in the all other parts of Application with injections. Next step, initialized Event Manager and Dispatcher are getting filled with Events and Observers depended on configuration. And the final step, Dispatcher dispatches Events one-by-one.

On the dispatching process, Application Strategy builds Request, Response and View, depending on selected strategy (like CLI, Http, REST etc - currently Http and Cli are done). Router resolving what route is invoked, and builds the MatchedRoute object. After that, depending on this information, Module and Controller are getting built, and after that, appropriate Action is running. If suitable Route is not found, Router builds PageNotFond MatchedRoute object. Response is getting filled with data in Action, and after that Controller sends the Response back to the client.

## FrontEnd ##

FrontEnd is built on **JavaScript/Marionette** as a single-page application by AMD architecture, and loaded with **require.js**.
Data is recieving with REST requests. 
Dependencies are stored in the bower, but all the frontend components are in the git repository already so no installation needed.

FrontEnd components are located in:

* **public/app**, **public/images**, **public/vendor**

## Demo ##

Check out the live demo version:

* [http://nvertisett.ukrone.net/](http://nvertisett.ukrone.net/)

*Please notice, right now any validations are done, not frontend or backend.*

another useful link is 

* [http://nvertisett.ukrone.net/test/observers](http://nvertisett.ukrone.net/test/observers)

this link is related with task content where events and observers are stored in the database, and by the application flow, registering to EventManager.
This page content is an output of triggered in Action Observers restored from database in the Module-related functions (see **application/Module/Test** module).

## Local Setup ##

get a local copy of this Application with:


```
#!bash

git clone https://alexey_ml@bitbucket.org/alexey_ml/nvertisett.git
```

it is in the Public access so no password is required.

Also to make it working on your local environment, create database with:


```
#!mysql

CREATE DATABASE `nvertisett` /*!40100 DEFAULT CHARACTER SET utf8 */
```

set all the database required (dsn, login, password) information in the **application/config/config.php** file

and then run this command from the site root which will create tables and add the initial data to tables: 


```
#!bash

php console/initdb.php
```

I have added the simple PDO wrapper to speed up the development: http://www.imavex.com/php-pdo-wrapper-class/
It is a bit of low quality as I understood in process, but it is able to run the neccessary actions for this test task.

Also bring some configuration for your web server to run this application from the web. Here is an example of Apache and nginx configs:


**Apache:**
```
#!conf

<VirtualHost *:8081>
        ServerName nvertisett.ukrone.net
        DocumentRoot /home/nvertisett/www/public/
        <Directory />
            Options all
            AllowOverride all
        </Directory>
        SetEnv APPLICATION_ENV development
        CustomLog /home/nvertisett/logs/nvertisett.access.log combined
        ErrorLog /home/nvertisett/logs/nvertisett.error.log
        ServerAdmin admin@nvertisett.local
</VirtualHost>
```

nginx:

```
#!conf

server {
  listen *:80;
  server_name nvertisett.ukrone.net;
  client_max_body_size 200M;
  access_log /home/nvertisett/logs/nginx.access.log;
  error_log /home/nvertisett/logs/logs/nginx.error.log;
  open_file_cache_errors off;
       
  location / {
    proxy_pass http://localhost:8081;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_connect_timeout 120;
    proxy_send_timeout 120;
    proxy_read_timeout 180;
  }
  location ~* \.(jpg|jpeg|gif|png|ico|css|bmp|swf|js|txt)$ {
    root /home/nvertisett/www/public/;
  }

}
```

after that, site should be accessible with the selected local or real domain.

## Unit Tests ##

run tests with this command:


```
#!bash

phpunit application/Tests/
```

Expected output is:

```
#!bash

phpunit application/Tests/
PHPUnit 4.8.26 by Sebastian Bergmann and contributors.

........

Time: 1.76 seconds, Memory: 14.00MB

OK (8 tests, 29 assertions)

```


here is a pretty big ammount of code, not covered with tests initially at all, so I was unable to cover the whole application with unit tests, I wrote test for a few components and for EventManager mainly as it was required in the task. Also I got some ideas how to create functional tests but it will take additional time. Also I got ideas about of refactoring while writing the tests, in the constants part especially, but it will take some additional time too, this moment its good enought to complete the task.

php version of my environment is: **PHP 5.5.9-1ubuntu4.16**